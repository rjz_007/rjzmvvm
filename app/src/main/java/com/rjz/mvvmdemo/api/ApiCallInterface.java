package com.rjz.mvvmdemo.api;

import com.rjz.mvvmdemo.ui.paging.model.MainResp;
import com.rjz.mvvmdemo.utils.AppConstant;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiCallInterface {

    @GET(AppConstant.URL_ANSWERS)
    Observable<MainResp> fetchPageWiseAnswers(
            @Query("page") Integer page,
            @Query("pagesize") Integer pagesize,
            @Query("site") String site);

    @GET(AppConstant.URL_QUESTION)
    Observable<MainResp> fetchQuestion(
            @Query("order") String order,
            @Query("sort") String sort,
            @Query("site") String site);
}
