package com.rjz.mvvmdemo.utils;

public class AppConstant {

    //Base url here
    public static final String BASE_URL = "https://api.stackexchange.com/2.2/";
    /* Api calls end poits*/
    public static final String URL_ANSWERS = "answers";
    public static final String URL_QUESTION = "questions";

    public final static String CHECK_NETWORK_ERROR = "Please Check your internet connection.";
    public static final Integer PAGE_SIZE = 50;
    public static final String SITE_NAME = "stackoverflow";

    // Page number starts with...
    public static int PAGE_INDEX = 1;
}
