package com.rjz.mvvmdemo;

import android.app.Application;
import android.content.Context;

import com.rjz.mvvmdemo.di.AppComponent;
import com.rjz.mvvmdemo.di.DaggerAppComponent;
import com.rjz.mvvmdemo.di.RoomModule;
import com.rjz.mvvmdemo.di.UtilsModule;

public class MyApplication extends Application {

    AppComponent appComponent;
    Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
        appComponent = DaggerAppComponent.builder()
                .utilsModule(new UtilsModule())
                .roomModule(new RoomModule(context))
                .build();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(context);
    }
}
