package com.rjz.mvvmdemo.repository;

import com.rjz.mvvmdemo.db.MyRoomDatabase;
import com.rjz.mvvmdemo.db.Person;

import java.util.List;

import io.reactivex.Observable;

public class RoomRepository {

    private MyRoomDatabase database;

    public RoomRepository(MyRoomDatabase databaseClient) {
        this.database = databaseClient;
    }

    /* Fetch all records available in DB */
    public Observable<List<Person>> getAllPersons() {
        return database.taskDao().getPersons();
    }

    /* Add new record in DB*/
    public void AddPerson(Person person) {
        database.taskDao().addPerson(person);
    }

}
