package com.rjz.mvvmdemo.repository;

import com.rjz.mvvmdemo.api.ApiCallInterface;
import com.rjz.mvvmdemo.ui.paging.model.MainResp;
import com.rjz.mvvmdemo.utils.AppConstant;

import io.reactivex.Observable;

public class ApiRepository {

    private ApiCallInterface apiCallInterface;

    public ApiRepository(ApiCallInterface apiCallInterface) {
        this.apiCallInterface = apiCallInterface;
    }

    /*
     * method to fetch page wise data of ANSWERS
     */
    public Observable<MainResp> executeNewsApi(int index) {
        return apiCallInterface.fetchPageWiseAnswers(index, AppConstant.PAGE_SIZE, AppConstant.SITE_NAME);
    }

    /*
     * method to fetch page wise data of all Questions
     */
    public Observable<MainResp> fetchData() {
        return apiCallInterface.fetchQuestion("desc", "activity", AppConstant.SITE_NAME);
    }
}
