package com.rjz.mvvmdemo.di;

import android.content.Context;

import androidx.room.Room;

import com.rjz.mvvmdemo.db.MyRoomDatabase;
import com.rjz.mvvmdemo.repository.RoomRepository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class RoomModule {

    private final MyRoomDatabase myDatabase;

    public RoomModule(Context context) {
        this.myDatabase = Room.databaseBuilder(context,
                MyRoomDatabase.class, "MyPersonDB")
                .build();
    }

    @Provides
    @Singleton
    RoomRepository getRepository() {
        return new RoomRepository(myDatabase);
    }
}
