package com.rjz.mvvmdemo.di;


import androidx.fragment.app.FragmentActivity;

import com.rjz.mvvmdemo.ui.home.HomeActivity;
import com.rjz.mvvmdemo.ui.paging.view.MainActivity;
import com.rjz.mvvmdemo.ui.retro.view.RetroActivity;
import com.rjz.mvvmdemo.ui.retro.view.RetroFragment;
import com.rjz.mvvmdemo.ui.roomact.view.RoomActivity;

import javax.inject.Singleton;

import dagger.Component;


@Component(modules = {UtilsModule.class , RoomModule.class})
@Singleton
public interface AppComponent {
    void doInjection(MainActivity mainActivity);
    void doInjection(RetroActivity retroActivity);
    void doInjection(HomeActivity homeActivity);
    void doInjection(RoomActivity roomActivity);
    void doInjection(RetroFragment retroFragment);
}
