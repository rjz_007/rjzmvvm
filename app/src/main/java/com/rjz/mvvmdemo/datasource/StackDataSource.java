package com.rjz.mvvmdemo.datasource;

import android.annotation.SuppressLint;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.paging.PageKeyedDataSource;

import com.rjz.mvvmdemo.repository.ApiRepository;
import com.rjz.mvvmdemo.ui.paging.model.Item;
import com.rjz.mvvmdemo.utils.AppConstant;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;


public class StackDataSource extends PageKeyedDataSource<Integer, Item> {

    private ApiRepository repository;
    private MutableLiveData<Boolean> isLoading = new MutableLiveData<>();
    private CompositeDisposable compositeDisposable;

    StackDataSource(ApiRepository repository, CompositeDisposable compositeDisposable) {
        this.repository = repository;
        this.compositeDisposable = compositeDisposable;
        isLoading = new MutableLiveData<>();
    }

    public MutableLiveData<Boolean> getIsLoading() {
        return isLoading;
    }

    @SuppressLint("CheckResult")
    @Override
    public void loadInitial(@NonNull LoadInitialParams<Integer> params, @NonNull LoadInitialCallback<Integer, Item> callback) {
       Log.e("!_@_@","---load init---");
        repository.executeNewsApi(AppConstant.PAGE_INDEX)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> {
                    compositeDisposable.add(disposable);
                    isLoading.postValue(true);
                }).subscribe(MainResp -> {
                    isLoading.postValue(false);
                    AppConstant.PAGE_INDEX++;
                    callback.onResult(MainResp.getItems(), null, AppConstant.PAGE_INDEX);
                },
                Throwable -> {
                    isLoading.postValue(false);
                });
    }

    @Override
    public void loadBefore(@NonNull LoadParams<Integer> params, @NonNull LoadCallback<Integer, Item> callback) {

    }

    @SuppressLint("CheckResult")
    @Override
    public void loadAfter(@NonNull LoadParams<Integer> params, @NonNull LoadCallback<Integer, Item> callback) {
        Log.e("!_@_@","---load After---");

        repository.executeNewsApi(params.key)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> {
                    compositeDisposable.add(disposable);
                    isLoading.postValue(true);
                }).subscribe(MainResp -> {
                    isLoading.postValue(false);
                    AppConstant.PAGE_INDEX++;
                    callback.onResult(MainResp.getItems(), MainResp.getHasMore() ? params.key + 1 : null);
                },
                Throwable -> {
                    isLoading.postValue(false);
                });
    }
}
