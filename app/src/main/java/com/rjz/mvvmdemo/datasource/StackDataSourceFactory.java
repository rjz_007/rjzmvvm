package com.rjz.mvvmdemo.datasource;

import androidx.lifecycle.MutableLiveData;
import androidx.paging.DataSource;

import com.rjz.mvvmdemo.ui.paging.model.Item;
import com.rjz.mvvmdemo.repository.ApiRepository;

import io.reactivex.disposables.CompositeDisposable;

public class StackDataSourceFactory extends DataSource.Factory<Integer, Item> {

    private MutableLiveData<StackDataSource> liveData;
    private ApiRepository repository;
    private CompositeDisposable compositeDisposable;

    public StackDataSourceFactory(ApiRepository repository, CompositeDisposable compositeDisposable) {
        this.repository = repository;
        this.compositeDisposable = compositeDisposable;
        liveData = new MutableLiveData<>();
    }

    public MutableLiveData<StackDataSource> getMutableLiveData() {
        return liveData;
    }

    @Override
    public DataSource<Integer, Item> create() {
        StackDataSource dataSourceClass = new StackDataSource(repository, compositeDisposable);
        liveData.postValue(dataSourceClass);
        return dataSourceClass;
    }
}
