package com.rjz.mvvmdemo.base;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.rjz.mvvmdemo.repository.ApiRepository;
import com.rjz.mvvmdemo.repository.RoomRepository;
import com.rjz.mvvmdemo.ui.paging.viewmodel.StackPagerViewModel;
import com.rjz.mvvmdemo.ui.retro.viewmodel.FragViewModel;
import com.rjz.mvvmdemo.ui.retro.viewmodel.RetroViewModel;
import com.rjz.mvvmdemo.ui.roomact.viewmodel.RoomViewModel;

import javax.inject.Inject;

/*
 * manage ViewModel Factory with Multi-binding.
 * */
public class ViewModelFactory implements ViewModelProvider.Factory {

    @Inject
    RoomRepository roomRepository;

    @Inject
    ApiRepository repository;

    @Inject
    public ViewModelFactory() {
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(StackPagerViewModel.class)) {
            return (T) new StackPagerViewModel(repository);
        } else if (modelClass.isAssignableFrom(RetroViewModel.class)) {
            return (T) new RetroViewModel(repository);
        } else if (modelClass.isAssignableFrom(RoomViewModel.class)) {
            return (T) new RoomViewModel(roomRepository);
        } else if (modelClass.isAssignableFrom(FragViewModel.class)) {
            return (T) new FragViewModel(repository);
        }
        throw new IllegalArgumentException("Unknown class name , please assign ViewModel here..");
    }
}
