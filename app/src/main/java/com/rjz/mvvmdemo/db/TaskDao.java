package com.rjz.mvvmdemo.db;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import io.reactivex.Observable;

@Dao
public interface TaskDao {

    @Query("Select * from person")
    Observable<List<Person>> getPersons();

    @Insert
    void addPerson(Person person);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void updatePerson(Person person);

}
