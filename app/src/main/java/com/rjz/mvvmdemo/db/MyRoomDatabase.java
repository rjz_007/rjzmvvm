package com.rjz.mvvmdemo.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {Person.class}, version = 1)
public abstract class MyRoomDatabase extends RoomDatabase {

    public abstract TaskDao taskDao();

}
