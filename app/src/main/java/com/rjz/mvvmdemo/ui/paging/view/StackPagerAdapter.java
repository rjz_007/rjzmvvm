package com.rjz.mvvmdemo.ui.paging.view;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.rjz.mvvmdemo.R;
import com.rjz.mvvmdemo.ui.paging.model.Item;
import com.rjz.mvvmdemo.ui.paging.model.MainResp;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class StackPagerAdapter extends PagedListAdapter<Item, StackPagerAdapter.MyViewHolder> {

    StackPagerAdapter() {
        super(MainResp.DIFF_CALLBACK);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_layout, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {
        holder.news_title.setText(getItem(position).getOwner().getDisplayName());
        Picasso.get().load(getItem(position).getOwner().getProfileImage()).into(holder.profile_image);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        CircleImageView profile_image;
        TextView news_title;

        MyViewHolder(View itemView) {
            super(itemView);
            profile_image = itemView.findViewById(R.id.profile_image);
            news_title = itemView.findViewById(R.id.news_title);
        }

    }
}
