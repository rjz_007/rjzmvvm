package com.rjz.mvvmdemo.ui.roomact.viewmodel;

import android.annotation.SuppressLint;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.rjz.mvvmdemo.db.Person;
import com.rjz.mvvmdemo.repository.RoomRepository;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class RoomViewModel extends ViewModel {

    private final RoomRepository roomRepository;
    public MutableLiveData<Boolean> isLoading = new MutableLiveData<>();
    public MutableLiveData<String> isError = new MutableLiveData<>();
    public MutableLiveData<List<Person>> itemList = new MutableLiveData<>();

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public RoomViewModel(RoomRepository roomRepository) {
        this.roomRepository = roomRepository;
    }

    @SuppressLint("CheckResult")
    public void init() {
        isLoading.postValue(true);

        roomRepository.getAllPersons()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> {
                    compositeDisposable.add(disposable);
                    isLoading.postValue(true);
                })
                .subscribe(mainResp -> {
                    isLoading.postValue(false);
                    int size = mainResp.size();
                    Log.e("!_@_@Size : ", size + " che...");
                    itemList.postValue(mainResp);
                }, Throwable -> {
                    isLoading.postValue(false);
                    isError.postValue("something went wrong.");
                });
    }

    @SuppressLint("CheckResult")
    public void addNewData(String name) {

        Completable.fromAction(() ->
                roomRepository.AddPerson(new Person(name, "title is test")))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> {
                    compositeDisposable.add(disposable);
                    isLoading.postValue(true);
                }).subscribe(
                new CompletableObserver() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onComplete() {
                        isLoading.postValue(false);
                    }

                    @Override
                    public void onError(Throwable e) {
                        isLoading.postValue(false);
                    }
                });
    }
}
