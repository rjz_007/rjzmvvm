package com.rjz.mvvmdemo.ui.roomact.view;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.rjz.mvvmdemo.MyApplication;
import com.rjz.mvvmdemo.R;
import com.rjz.mvvmdemo.base.ViewModelFactory;
import com.rjz.mvvmdemo.databinding.ActivityRoomBinding;
import com.rjz.mvvmdemo.db.Person;
import com.rjz.mvvmdemo.repository.RoomRepository;
import com.rjz.mvvmdemo.ui.roomact.viewmodel.RoomViewModel;

import javax.inject.Inject;

public class RoomActivity extends AppCompatActivity {

    @Inject
    ViewModelFactory viewModelFactory;


    @Inject
    RoomRepository roomRepository;

    private String TAG = "!_@_RoomAct : ";
    private ActivityRoomBinding binding;
    private RoomViewModel roomViewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_room);
        ((MyApplication) getApplication()).getAppComponent().doInjection(this);
        roomViewModel = ViewModelProviders.of(this, viewModelFactory).get(RoomViewModel.class);

        roomViewModel.init();

        roomViewModel.isLoading.observe(this, isLoading -> {
            Log.e(TAG, isLoading + " -isLoading-- ");
        });
        roomViewModel.isError.observe(this, error -> {
            Log.e(TAG, error + " -error-- ");
            Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
        });
        roomViewModel.itemList.observe(this, itemList -> {
            Log.e(TAG, " ---fetched all data------ ");
            for (Person itm : itemList) {
                Log.e(TAG, "--Title-> " + itm.getTitle());
            }
        });

        binding.btnAdd.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("CheckResult")
            @Override
            public void onClick(View v) {
                roomViewModel.addNewData(binding.edtName.getText().toString());
            }
        });
    }
}
