package com.rjz.mvvmdemo.ui.paging.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PagedList;

import com.rjz.mvvmdemo.datasource.StackDataSource;
import com.rjz.mvvmdemo.datasource.StackDataSourceFactory;
import com.rjz.mvvmdemo.repository.ApiRepository;
import com.rjz.mvvmdemo.ui.paging.model.Item;
import com.rjz.mvvmdemo.utils.AppConstant;

import io.reactivex.disposables.CompositeDisposable;

public class StackPagerViewModel extends ViewModel {

    private StackDataSourceFactory stackDataSourceFactory;
    private LiveData<PagedList<Item>> pagedListData = new MutableLiveData<>();

    private LiveData<Boolean> isLoading = new MutableLiveData<>();
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public StackPagerViewModel(ApiRepository repository) {
        stackDataSourceFactory = new StackDataSourceFactory(repository, compositeDisposable);
        initializePaging();
    }

    private void initializePaging() {

        PagedList.Config pagedListConfig =
                new PagedList.Config.Builder()
                        .setEnablePlaceholders(false)
//                        .setInitialLoadSizeHint(10)
                        .setPageSize(AppConstant.PAGE_SIZE)
                        .build();

        pagedListData = new LivePagedListBuilder<>(stackDataSourceFactory, pagedListConfig)
                .build();

        isLoading = Transformations.switchMap(stackDataSourceFactory.getMutableLiveData(),
                StackDataSource::getIsLoading);
    }

    public LiveData<Boolean> getIsLoading() {
        return isLoading;
    }

    public LiveData<PagedList<Item>> getPagedListData() {
        return pagedListData;
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        compositeDisposable.clear();
    }
}