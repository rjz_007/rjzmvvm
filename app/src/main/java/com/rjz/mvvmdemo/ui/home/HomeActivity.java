package com.rjz.mvvmdemo.ui.home;

import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;

import com.rjz.mvvmdemo.MyApplication;
import com.rjz.mvvmdemo.R;
import com.rjz.mvvmdemo.databinding.ActivityHomeBinding;
import com.rjz.mvvmdemo.ui.paging.view.MainActivity;
import com.rjz.mvvmdemo.ui.retro.view.RetroActivity;
import com.rjz.mvvmdemo.ui.roomact.view.RoomActivity;

public class HomeActivity extends AppCompatActivity {

    private ActivityHomeBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_home);
        ((MyApplication) getApplication()).getAppComponent().doInjection(this);
        binding.btnPaging.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeActivity.this, MainActivity.class));
            }
        });
        binding.btnNormalList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeActivity.this, RetroActivity.class));
            }
        });
        binding.btnRoomList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeActivity.this, RoomActivity.class));
            }
        });

    }
}
