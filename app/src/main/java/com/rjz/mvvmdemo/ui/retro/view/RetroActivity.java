package com.rjz.mvvmdemo.ui.retro.view;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;

import com.rjz.mvvmdemo.MyApplication;
import com.rjz.mvvmdemo.R;
import com.rjz.mvvmdemo.base.ViewModelFactory;
import com.rjz.mvvmdemo.databinding.ActivityRetroBinding;
import com.rjz.mvvmdemo.ui.paging.model.Item;
import com.rjz.mvvmdemo.ui.retro.viewmodel.RetroViewModel;

import javax.inject.Inject;

public class RetroActivity extends AppCompatActivity {

    @Inject
    ViewModelFactory viewModelFactory;

    private String TAG = "!_@_RetroAct : ";
    private ActivityRetroBinding binding;
    private RetroViewModel retroViewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_retro);
        ((MyApplication) getApplication()).getAppComponent().doInjection(this);
        retroViewModel = ViewModelProviders.of(this, viewModelFactory).get(RetroViewModel.class);

        binding.btnFrg.setOnClickListener(View -> {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.llContainer, new RetroFragment(), "RetroFragment");
            ft.commit();
        });

        retroViewModel.isLoading.observe(this, isLoading -> {
            Log.e(TAG, isLoading + " -isLoading-- ");
        });

        retroViewModel.isError.observe(this, error -> {
            Log.e(TAG, error + " -error-- ");
            Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
        });

        retroViewModel.itemList.observe(this, itemList -> {
            Log.e(TAG, " ---fetched all data------ ");
            for (Item itm : itemList) {
                Log.e(TAG, "--Title-> " + itm.getQuestionTitle());
            }
        });
    }
}
