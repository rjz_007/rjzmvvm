package com.rjz.mvvmdemo.ui.paging.view;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.android.material.snackbar.Snackbar;
import com.rjz.mvvmdemo.MyApplication;
import com.rjz.mvvmdemo.R;
import com.rjz.mvvmdemo.base.ViewModelFactory;
import com.rjz.mvvmdemo.databinding.ActivityPagingBinding;
import com.rjz.mvvmdemo.ui.paging.viewmodel.StackPagerViewModel;
import com.rjz.mvvmdemo.utils.AppConstant;
import com.rjz.mvvmdemo.utils.Utility;

import java.util.Objects;

import javax.inject.Inject;

public class MainActivity extends AppCompatActivity {

    @Inject
    ViewModelFactory viewModelFactory;

    StackPagerViewModel viewModel;

    ActivityPagingBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_paging);
        ((MyApplication) getApplication()).getAppComponent().doInjection(this);
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(StackPagerViewModel.class);
        init();
    }

    private void init() {

        binding.list.setLayoutManager(new LinearLayoutManager(this));
        StackPagerAdapter adapter = new StackPagerAdapter();
        binding.list.setAdapter(adapter);

        if (!Utility.checkInternetConnection(this)) {
            Snackbar.make(findViewById(android.R.id.content), AppConstant.CHECK_NETWORK_ERROR, Snackbar.LENGTH_SHORT)
                    .show();
        }

        viewModel.getPagedListData().observe(this, adapter::submitList);

        viewModel.getIsLoading().observe(this, status -> {
            if (Objects.requireNonNull(status)) {
                binding.progress.setVisibility(View.VISIBLE);
            } else {
                binding.progress.setVisibility(View.GONE);
            }
        });

    }
}
