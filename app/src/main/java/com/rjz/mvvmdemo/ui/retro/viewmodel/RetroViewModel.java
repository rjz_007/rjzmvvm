package com.rjz.mvvmdemo.ui.retro.viewmodel;

import android.annotation.SuppressLint;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import android.util.Log;

import com.rjz.mvvmdemo.ui.paging.model.Item;
import com.rjz.mvvmdemo.repository.ApiRepository;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class RetroViewModel extends ViewModel {

    public MutableLiveData<Boolean> isLoading = new MutableLiveData<>();
    public MutableLiveData<String> isError = new MutableLiveData<>();
    public MutableLiveData<List<Item>> itemList = new MutableLiveData<>();

    private ApiRepository repository;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public RetroViewModel(ApiRepository repository) {
        this.repository = repository;
        init();
    }

    @SuppressLint("CheckResult")
    private void init() {
        isLoading.postValue(true);

        repository.fetchData()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> {
                    compositeDisposable.add(disposable);
                    isLoading.postValue(true);
                })
                .subscribe(mainResp -> {
                    isLoading.postValue(false);
                    int size = mainResp.getItems().size();
                    Log.e("!_@_@Size : ", size + " che...");
                    itemList.postValue(mainResp.getItems());
                }, Throwable -> {
                    isLoading.postValue(false);
                    isError.postValue("something went wrong.");
                });
    }


}
