package com.rjz.mvvmdemo.ui.roomact.view;

import com.rjz.mvvmdemo.base.ViewModelFactory;
import com.rjz.mvvmdemo.repository.RoomRepository;
import dagger.MembersInjector;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class RoomActivity_MembersInjector implements MembersInjector<RoomActivity> {
  private final Provider<ViewModelFactory> viewModelFactoryProvider;

  private final Provider<RoomRepository> roomRepositoryProvider;

  public RoomActivity_MembersInjector(
      Provider<ViewModelFactory> viewModelFactoryProvider,
      Provider<RoomRepository> roomRepositoryProvider) {
    this.viewModelFactoryProvider = viewModelFactoryProvider;
    this.roomRepositoryProvider = roomRepositoryProvider;
  }

  public static MembersInjector<RoomActivity> create(
      Provider<ViewModelFactory> viewModelFactoryProvider,
      Provider<RoomRepository> roomRepositoryProvider) {
    return new RoomActivity_MembersInjector(viewModelFactoryProvider, roomRepositoryProvider);
  }

  @Override
  public void injectMembers(RoomActivity instance) {
    injectViewModelFactory(instance, viewModelFactoryProvider.get());
    injectRoomRepository(instance, roomRepositoryProvider.get());
  }

  public static void injectViewModelFactory(
      RoomActivity instance, ViewModelFactory viewModelFactory) {
    instance.viewModelFactory = viewModelFactory;
  }

  public static void injectRoomRepository(RoomActivity instance, RoomRepository roomRepository) {
    instance.roomRepository = roomRepository;
  }
}
