package com.rjz.mvvmdemo.base;

import com.rjz.mvvmdemo.repository.ApiRepository;
import com.rjz.mvvmdemo.repository.RoomRepository;
import dagger.MembersInjector;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class ViewModelFactory_MembersInjector implements MembersInjector<ViewModelFactory> {
  private final Provider<RoomRepository> roomRepositoryProvider;

  private final Provider<ApiRepository> repositoryProvider;

  public ViewModelFactory_MembersInjector(
      Provider<RoomRepository> roomRepositoryProvider, Provider<ApiRepository> repositoryProvider) {
    this.roomRepositoryProvider = roomRepositoryProvider;
    this.repositoryProvider = repositoryProvider;
  }

  public static MembersInjector<ViewModelFactory> create(
      Provider<RoomRepository> roomRepositoryProvider, Provider<ApiRepository> repositoryProvider) {
    return new ViewModelFactory_MembersInjector(roomRepositoryProvider, repositoryProvider);
  }

  @Override
  public void injectMembers(ViewModelFactory instance) {
    injectRoomRepository(instance, roomRepositoryProvider.get());
    injectRepository(instance, repositoryProvider.get());
  }

  public static void injectRoomRepository(
      ViewModelFactory instance, RoomRepository roomRepository) {
    instance.roomRepository = roomRepository;
  }

  public static void injectRepository(ViewModelFactory instance, ApiRepository repository) {
    instance.repository = repository;
  }
}
