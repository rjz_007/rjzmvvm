package com.rjz.mvvmdemo.di;

import com.google.gson.Gson;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import javax.inject.Provider;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class UtilsModule_ProvideRetrofitFactory implements Factory<Retrofit> {
  private final UtilsModule module;

  private final Provider<Gson> gsonProvider;

  private final Provider<OkHttpClient> okHttpClientProvider;

  public UtilsModule_ProvideRetrofitFactory(
      UtilsModule module,
      Provider<Gson> gsonProvider,
      Provider<OkHttpClient> okHttpClientProvider) {
    this.module = module;
    this.gsonProvider = gsonProvider;
    this.okHttpClientProvider = okHttpClientProvider;
  }

  @Override
  public Retrofit get() {
    return provideRetrofit(module, gsonProvider.get(), okHttpClientProvider.get());
  }

  public static UtilsModule_ProvideRetrofitFactory create(
      UtilsModule module,
      Provider<Gson> gsonProvider,
      Provider<OkHttpClient> okHttpClientProvider) {
    return new UtilsModule_ProvideRetrofitFactory(module, gsonProvider, okHttpClientProvider);
  }

  public static Retrofit provideRetrofit(
      UtilsModule instance, Gson gson, OkHttpClient okHttpClient) {
    return Preconditions.checkNotNull(
        instance.provideRetrofit(gson, okHttpClient),
        "Cannot return null from a non-@Nullable @Provides method");
  }
}
