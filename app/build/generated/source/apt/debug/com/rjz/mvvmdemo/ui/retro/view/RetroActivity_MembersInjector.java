package com.rjz.mvvmdemo.ui.retro.view;

import com.rjz.mvvmdemo.base.ViewModelFactory;
import dagger.MembersInjector;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class RetroActivity_MembersInjector implements MembersInjector<RetroActivity> {
  private final Provider<ViewModelFactory> viewModelFactoryProvider;

  public RetroActivity_MembersInjector(Provider<ViewModelFactory> viewModelFactoryProvider) {
    this.viewModelFactoryProvider = viewModelFactoryProvider;
  }

  public static MembersInjector<RetroActivity> create(
      Provider<ViewModelFactory> viewModelFactoryProvider) {
    return new RetroActivity_MembersInjector(viewModelFactoryProvider);
  }

  @Override
  public void injectMembers(RetroActivity instance) {
    injectViewModelFactory(instance, viewModelFactoryProvider.get());
  }

  public static void injectViewModelFactory(
      RetroActivity instance, ViewModelFactory viewModelFactory) {
    instance.viewModelFactory = viewModelFactory;
  }
}
