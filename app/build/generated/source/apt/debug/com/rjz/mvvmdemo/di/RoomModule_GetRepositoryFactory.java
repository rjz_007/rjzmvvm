package com.rjz.mvvmdemo.di;

import com.rjz.mvvmdemo.repository.RoomRepository;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class RoomModule_GetRepositoryFactory implements Factory<RoomRepository> {
  private final RoomModule module;

  public RoomModule_GetRepositoryFactory(RoomModule module) {
    this.module = module;
  }

  @Override
  public RoomRepository get() {
    return getRepository(module);
  }

  public static RoomModule_GetRepositoryFactory create(RoomModule module) {
    return new RoomModule_GetRepositoryFactory(module);
  }

  public static RoomRepository getRepository(RoomModule instance) {
    return Preconditions.checkNotNull(
        instance.getRepository(), "Cannot return null from a non-@Nullable @Provides method");
  }
}
