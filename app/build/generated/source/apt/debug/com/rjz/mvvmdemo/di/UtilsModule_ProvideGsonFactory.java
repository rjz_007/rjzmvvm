package com.rjz.mvvmdemo.di;

import com.google.gson.Gson;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class UtilsModule_ProvideGsonFactory implements Factory<Gson> {
  private final UtilsModule module;

  public UtilsModule_ProvideGsonFactory(UtilsModule module) {
    this.module = module;
  }

  @Override
  public Gson get() {
    return provideGson(module);
  }

  public static UtilsModule_ProvideGsonFactory create(UtilsModule module) {
    return new UtilsModule_ProvideGsonFactory(module);
  }

  public static Gson provideGson(UtilsModule instance) {
    return Preconditions.checkNotNull(
        instance.provideGson(), "Cannot return null from a non-@Nullable @Provides method");
  }
}
