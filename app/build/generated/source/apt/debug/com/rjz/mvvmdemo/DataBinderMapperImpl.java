package com.rjz.mvvmdemo;

import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.DataBinderMapper;
import androidx.databinding.DataBindingComponent;
import androidx.databinding.ViewDataBinding;
import com.rjz.mvvmdemo.databinding.ActivityHomeBindingImpl;
import com.rjz.mvvmdemo.databinding.ActivityPagingBindingImpl;
import com.rjz.mvvmdemo.databinding.ActivityRetroBindingImpl;
import com.rjz.mvvmdemo.databinding.ActivityRoomBindingImpl;
import com.rjz.mvvmdemo.databinding.RowLayoutBindingImpl;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Object;
import java.lang.Override;
import java.lang.RuntimeException;
import java.lang.String;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.annotation.Generated;

@Generated("Android Data Binding")
public class DataBinderMapperImpl extends DataBinderMapper {
  private static final int LAYOUT_ACTIVITYHOME = 1;

  private static final int LAYOUT_ACTIVITYPAGING = 2;

  private static final int LAYOUT_ACTIVITYRETRO = 3;

  private static final int LAYOUT_ACTIVITYROOM = 4;

  private static final int LAYOUT_ROWLAYOUT = 5;

  private static final SparseIntArray INTERNAL_LAYOUT_ID_LOOKUP = new SparseIntArray(5);

  static {
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.rjz.mvvmdemo.R.layout.activity_home, LAYOUT_ACTIVITYHOME);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.rjz.mvvmdemo.R.layout.activity_paging, LAYOUT_ACTIVITYPAGING);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.rjz.mvvmdemo.R.layout.activity_retro, LAYOUT_ACTIVITYRETRO);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.rjz.mvvmdemo.R.layout.activity_room, LAYOUT_ACTIVITYROOM);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.rjz.mvvmdemo.R.layout.row_layout, LAYOUT_ROWLAYOUT);
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View view, int layoutId) {
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = view.getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
        case  LAYOUT_ACTIVITYHOME: {
          if ("layout/activity_home_0".equals(tag)) {
            return new ActivityHomeBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_home is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYPAGING: {
          if ("layout/activity_paging_0".equals(tag)) {
            return new ActivityPagingBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_paging is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYRETRO: {
          if ("layout/activity_retro_0".equals(tag)) {
            return new ActivityRetroBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_retro is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYROOM: {
          if ("layout/activity_room_0".equals(tag)) {
            return new ActivityRoomBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_room is invalid. Received: " + tag);
        }
        case  LAYOUT_ROWLAYOUT: {
          if ("layout/row_layout_0".equals(tag)) {
            return new RowLayoutBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for row_layout is invalid. Received: " + tag);
        }
      }
    }
    return null;
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View[] views, int layoutId) {
    if(views == null || views.length == 0) {
      return null;
    }
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = views[0].getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
      }
    }
    return null;
  }

  @Override
  public int getLayoutId(String tag) {
    if (tag == null) {
      return 0;
    }
    Integer tmpVal = InnerLayoutIdLookup.sKeys.get(tag);
    return tmpVal == null ? 0 : tmpVal;
  }

  @Override
  public String convertBrIdToString(int localId) {
    String tmpVal = InnerBrLookup.sKeys.get(localId);
    return tmpVal;
  }

  @Override
  public List<DataBinderMapper> collectDependencies() {
    ArrayList<DataBinderMapper> result = new ArrayList<DataBinderMapper>(1);
    result.add(new androidx.databinding.library.baseAdapters.DataBinderMapperImpl());
    return result;
  }

  private static class InnerBrLookup {
    static final SparseArray<String> sKeys = new SparseArray<String>(2);

    static {
      sKeys.put(0, "_all");
    }
  }

  private static class InnerLayoutIdLookup {
    static final HashMap<String, Integer> sKeys = new HashMap<String, Integer>(5);

    static {
      sKeys.put("layout/activity_home_0", com.rjz.mvvmdemo.R.layout.activity_home);
      sKeys.put("layout/activity_paging_0", com.rjz.mvvmdemo.R.layout.activity_paging);
      sKeys.put("layout/activity_retro_0", com.rjz.mvvmdemo.R.layout.activity_retro);
      sKeys.put("layout/activity_room_0", com.rjz.mvvmdemo.R.layout.activity_room);
      sKeys.put("layout/row_layout_0", com.rjz.mvvmdemo.R.layout.row_layout);
    }
  }
}
