package com.rjz.mvvmdemo.di;

import androidx.lifecycle.ViewModelProvider;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class UtilsModule_GetViewModelFactoryFactory
    implements Factory<ViewModelProvider.Factory> {
  private final UtilsModule module;

  public UtilsModule_GetViewModelFactoryFactory(UtilsModule module) {
    this.module = module;
  }

  @Override
  public ViewModelProvider.Factory get() {
    return getViewModelFactory(module);
  }

  public static UtilsModule_GetViewModelFactoryFactory create(UtilsModule module) {
    return new UtilsModule_GetViewModelFactoryFactory(module);
  }

  public static ViewModelProvider.Factory getViewModelFactory(UtilsModule instance) {
    return Preconditions.checkNotNull(
        instance.getViewModelFactory(), "Cannot return null from a non-@Nullable @Provides method");
  }
}
