package com.rjz.mvvmdemo.base;

import com.rjz.mvvmdemo.repository.ApiRepository;
import com.rjz.mvvmdemo.repository.RoomRepository;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class ViewModelFactory_Factory implements Factory<ViewModelFactory> {
  private final Provider<RoomRepository> roomRepositoryProvider;

  private final Provider<ApiRepository> repositoryProvider;

  public ViewModelFactory_Factory(
      Provider<RoomRepository> roomRepositoryProvider, Provider<ApiRepository> repositoryProvider) {
    this.roomRepositoryProvider = roomRepositoryProvider;
    this.repositoryProvider = repositoryProvider;
  }

  @Override
  public ViewModelFactory get() {
    ViewModelFactory instance = new ViewModelFactory();
    ViewModelFactory_MembersInjector.injectRoomRepository(instance, roomRepositoryProvider.get());
    ViewModelFactory_MembersInjector.injectRepository(instance, repositoryProvider.get());
    return instance;
  }

  public static ViewModelFactory_Factory create(
      Provider<RoomRepository> roomRepositoryProvider, Provider<ApiRepository> repositoryProvider) {
    return new ViewModelFactory_Factory(roomRepositoryProvider, repositoryProvider);
  }

  public static ViewModelFactory newInstance() {
    return new ViewModelFactory();
  }
}
