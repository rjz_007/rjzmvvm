package com.rjz.mvvmdemo.di;

import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import okhttp3.OkHttpClient;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class UtilsModule_ProvideOkHttpClientFactory implements Factory<OkHttpClient> {
  private final UtilsModule module;

  public UtilsModule_ProvideOkHttpClientFactory(UtilsModule module) {
    this.module = module;
  }

  @Override
  public OkHttpClient get() {
    return provideOkHttpClient(module);
  }

  public static UtilsModule_ProvideOkHttpClientFactory create(UtilsModule module) {
    return new UtilsModule_ProvideOkHttpClientFactory(module);
  }

  public static OkHttpClient provideOkHttpClient(UtilsModule instance) {
    return Preconditions.checkNotNull(
        instance.provideOkHttpClient(), "Cannot return null from a non-@Nullable @Provides method");
  }
}
