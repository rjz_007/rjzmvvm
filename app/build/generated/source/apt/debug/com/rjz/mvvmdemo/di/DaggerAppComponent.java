package com.rjz.mvvmdemo.di;

import com.google.gson.Gson;
import com.rjz.mvvmdemo.api.ApiCallInterface;
import com.rjz.mvvmdemo.base.ViewModelFactory;
import com.rjz.mvvmdemo.base.ViewModelFactory_Factory;
import com.rjz.mvvmdemo.base.ViewModelFactory_MembersInjector;
import com.rjz.mvvmdemo.repository.ApiRepository;
import com.rjz.mvvmdemo.repository.RoomRepository;
import com.rjz.mvvmdemo.ui.home.HomeActivity;
import com.rjz.mvvmdemo.ui.paging.view.MainActivity;
import com.rjz.mvvmdemo.ui.paging.view.MainActivity_MembersInjector;
import com.rjz.mvvmdemo.ui.retro.view.RetroActivity;
import com.rjz.mvvmdemo.ui.retro.view.RetroActivity_MembersInjector;
import com.rjz.mvvmdemo.ui.roomact.view.RoomActivity;
import com.rjz.mvvmdemo.ui.roomact.view.RoomActivity_MembersInjector;
import dagger.internal.DoubleCheck;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import javax.inject.Provider;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class DaggerAppComponent implements AppComponent {
  private Provider<RoomRepository> getRepositoryProvider;

  private Provider<Gson> provideGsonProvider;

  private Provider<OkHttpClient> provideOkHttpClientProvider;

  private Provider<Retrofit> provideRetrofitProvider;

  private Provider<ApiCallInterface> getApiCallInterfaceProvider;

  private Provider<ApiRepository> getRepositoryProvider2;

  private DaggerAppComponent(UtilsModule utilsModuleParam, RoomModule roomModuleParam) {

    initialize(utilsModuleParam, roomModuleParam);
  }

  public static Builder builder() {
    return new Builder();
  }

  private ViewModelFactory getViewModelFactory() {
    return injectViewModelFactory(ViewModelFactory_Factory.newInstance());
  }

  @SuppressWarnings("unchecked")
  private void initialize(final UtilsModule utilsModuleParam, final RoomModule roomModuleParam) {
    this.getRepositoryProvider =
        DoubleCheck.provider(RoomModule_GetRepositoryFactory.create(roomModuleParam));
    this.provideGsonProvider =
        DoubleCheck.provider(UtilsModule_ProvideGsonFactory.create(utilsModuleParam));
    this.provideOkHttpClientProvider =
        DoubleCheck.provider(UtilsModule_ProvideOkHttpClientFactory.create(utilsModuleParam));
    this.provideRetrofitProvider =
        DoubleCheck.provider(
            UtilsModule_ProvideRetrofitFactory.create(
                utilsModuleParam, provideGsonProvider, provideOkHttpClientProvider));
    this.getApiCallInterfaceProvider =
        DoubleCheck.provider(
            UtilsModule_GetApiCallInterfaceFactory.create(
                utilsModuleParam, provideRetrofitProvider));
    this.getRepositoryProvider2 =
        DoubleCheck.provider(
            UtilsModule_GetRepositoryFactory.create(utilsModuleParam, getApiCallInterfaceProvider));
  }

  @Override
  public void doInjection(MainActivity mainActivity) {
    injectMainActivity(mainActivity);
  }

  @Override
  public void doInjection(RetroActivity retroActivity) {
    injectRetroActivity(retroActivity);
  }

  @Override
  public void doInjection(HomeActivity homeActivity) {}

  @Override
  public void doInjection(RoomActivity roomActivity) {
    injectRoomActivity(roomActivity);
  }

  private ViewModelFactory injectViewModelFactory(ViewModelFactory instance) {
    ViewModelFactory_MembersInjector.injectRoomRepository(instance, getRepositoryProvider.get());
    ViewModelFactory_MembersInjector.injectRepository(instance, getRepositoryProvider2.get());
    return instance;
  }

  private MainActivity injectMainActivity(MainActivity instance) {
    MainActivity_MembersInjector.injectViewModelFactory(instance, getViewModelFactory());
    return instance;
  }

  private RetroActivity injectRetroActivity(RetroActivity instance) {
    RetroActivity_MembersInjector.injectViewModelFactory(instance, getViewModelFactory());
    return instance;
  }

  private RoomActivity injectRoomActivity(RoomActivity instance) {
    RoomActivity_MembersInjector.injectViewModelFactory(instance, getViewModelFactory());
    RoomActivity_MembersInjector.injectRoomRepository(instance, getRepositoryProvider.get());
    return instance;
  }

  public static final class Builder {
    private UtilsModule utilsModule;

    private RoomModule roomModule;

    private Builder() {}

    public Builder utilsModule(UtilsModule utilsModule) {
      this.utilsModule = Preconditions.checkNotNull(utilsModule);
      return this;
    }

    public Builder roomModule(RoomModule roomModule) {
      this.roomModule = Preconditions.checkNotNull(roomModule);
      return this;
    }

    public AppComponent build() {
      if (utilsModule == null) {
        this.utilsModule = new UtilsModule();
      }
      Preconditions.checkBuilderRequirement(roomModule, RoomModule.class);
      return new DaggerAppComponent(utilsModule, roomModule);
    }
  }
}
