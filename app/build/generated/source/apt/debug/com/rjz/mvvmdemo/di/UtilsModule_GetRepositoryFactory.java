package com.rjz.mvvmdemo.di;

import com.rjz.mvvmdemo.api.ApiCallInterface;
import com.rjz.mvvmdemo.repository.ApiRepository;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class UtilsModule_GetRepositoryFactory implements Factory<ApiRepository> {
  private final UtilsModule module;

  private final Provider<ApiCallInterface> apiCallInterfaceProvider;

  public UtilsModule_GetRepositoryFactory(
      UtilsModule module, Provider<ApiCallInterface> apiCallInterfaceProvider) {
    this.module = module;
    this.apiCallInterfaceProvider = apiCallInterfaceProvider;
  }

  @Override
  public ApiRepository get() {
    return getRepository(module, apiCallInterfaceProvider.get());
  }

  public static UtilsModule_GetRepositoryFactory create(
      UtilsModule module, Provider<ApiCallInterface> apiCallInterfaceProvider) {
    return new UtilsModule_GetRepositoryFactory(module, apiCallInterfaceProvider);
  }

  public static ApiRepository getRepository(
      UtilsModule instance, ApiCallInterface apiCallInterface) {
    return Preconditions.checkNotNull(
        instance.getRepository(apiCallInterface),
        "Cannot return null from a non-@Nullable @Provides method");
  }
}
