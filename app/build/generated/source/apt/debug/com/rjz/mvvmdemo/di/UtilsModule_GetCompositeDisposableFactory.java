package com.rjz.mvvmdemo.di;

import dagger.internal.Factory;
import dagger.internal.Preconditions;
import io.reactivex.disposables.CompositeDisposable;
import javax.annotation.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class UtilsModule_GetCompositeDisposableFactory
    implements Factory<CompositeDisposable> {
  private final UtilsModule module;

  public UtilsModule_GetCompositeDisposableFactory(UtilsModule module) {
    this.module = module;
  }

  @Override
  public CompositeDisposable get() {
    return getCompositeDisposable(module);
  }

  public static UtilsModule_GetCompositeDisposableFactory create(UtilsModule module) {
    return new UtilsModule_GetCompositeDisposableFactory(module);
  }

  public static CompositeDisposable getCompositeDisposable(UtilsModule instance) {
    return Preconditions.checkNotNull(
        instance.getCompositeDisposable(),
        "Cannot return null from a non-@Nullable @Provides method");
  }
}
