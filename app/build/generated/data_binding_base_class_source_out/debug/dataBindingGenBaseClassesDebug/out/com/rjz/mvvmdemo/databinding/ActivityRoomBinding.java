package com.rjz.mvvmdemo.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class ActivityRoomBinding extends ViewDataBinding {
  @NonNull
  public final Button btnAdd;

  @NonNull
  public final EditText edtName;

  @NonNull
  public final TextView tvTEext;

  protected ActivityRoomBinding(Object _bindingComponent, View _root, int _localFieldCount,
      Button btnAdd, EditText edtName, TextView tvTEext) {
    super(_bindingComponent, _root, _localFieldCount);
    this.btnAdd = btnAdd;
    this.edtName = edtName;
    this.tvTEext = tvTEext;
  }

  @NonNull
  public static ActivityRoomBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.activity_room, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static ActivityRoomBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<ActivityRoomBinding>inflateInternal(inflater, com.rjz.mvvmdemo.R.layout.activity_room, root, attachToRoot, component);
  }

  @NonNull
  public static ActivityRoomBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.activity_room, null, false, component)
   */
  @NonNull
  @Deprecated
  public static ActivityRoomBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<ActivityRoomBinding>inflateInternal(inflater, com.rjz.mvvmdemo.R.layout.activity_room, null, false, component);
  }

  public static ActivityRoomBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static ActivityRoomBinding bind(@NonNull View view, @Nullable Object component) {
    return (ActivityRoomBinding)bind(component, view, com.rjz.mvvmdemo.R.layout.activity_room);
  }
}
