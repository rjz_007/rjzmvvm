package com.rjz.mvvmdemo.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import de.hdodenhof.circleimageview.CircleImageView;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class RowLayoutBinding extends ViewDataBinding {
  @NonNull
  public final TextView newsTitle;

  @NonNull
  public final CircleImageView profileImage;

  protected RowLayoutBinding(Object _bindingComponent, View _root, int _localFieldCount,
      TextView newsTitle, CircleImageView profileImage) {
    super(_bindingComponent, _root, _localFieldCount);
    this.newsTitle = newsTitle;
    this.profileImage = profileImage;
  }

  @NonNull
  public static RowLayoutBinding inflate(@NonNull LayoutInflater inflater, @Nullable ViewGroup root,
      boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.row_layout, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static RowLayoutBinding inflate(@NonNull LayoutInflater inflater, @Nullable ViewGroup root,
      boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<RowLayoutBinding>inflateInternal(inflater, com.rjz.mvvmdemo.R.layout.row_layout, root, attachToRoot, component);
  }

  @NonNull
  public static RowLayoutBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.row_layout, null, false, component)
   */
  @NonNull
  @Deprecated
  public static RowLayoutBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<RowLayoutBinding>inflateInternal(inflater, com.rjz.mvvmdemo.R.layout.row_layout, null, false, component);
  }

  public static RowLayoutBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static RowLayoutBinding bind(@NonNull View view, @Nullable Object component) {
    return (RowLayoutBinding)bind(component, view, com.rjz.mvvmdemo.R.layout.row_layout);
  }
}
