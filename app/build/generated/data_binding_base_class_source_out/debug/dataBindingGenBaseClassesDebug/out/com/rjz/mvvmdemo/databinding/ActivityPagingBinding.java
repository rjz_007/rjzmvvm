package com.rjz.mvvmdemo.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class ActivityPagingBinding extends ViewDataBinding {
  @NonNull
  public final RecyclerView list;

  @NonNull
  public final ProgressBar progress;

  protected ActivityPagingBinding(Object _bindingComponent, View _root, int _localFieldCount,
      RecyclerView list, ProgressBar progress) {
    super(_bindingComponent, _root, _localFieldCount);
    this.list = list;
    this.progress = progress;
  }

  @NonNull
  public static ActivityPagingBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.activity_paging, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static ActivityPagingBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<ActivityPagingBinding>inflateInternal(inflater, com.rjz.mvvmdemo.R.layout.activity_paging, root, attachToRoot, component);
  }

  @NonNull
  public static ActivityPagingBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.activity_paging, null, false, component)
   */
  @NonNull
  @Deprecated
  public static ActivityPagingBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<ActivityPagingBinding>inflateInternal(inflater, com.rjz.mvvmdemo.R.layout.activity_paging, null, false, component);
  }

  public static ActivityPagingBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static ActivityPagingBinding bind(@NonNull View view, @Nullable Object component) {
    return (ActivityPagingBinding)bind(component, view, com.rjz.mvvmdemo.R.layout.activity_paging);
  }
}
